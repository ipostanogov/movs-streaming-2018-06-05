name := "movs-streaming-2018-06-05"

version := "1.0"

scalaVersion := "2.11.12"

val sparkVersion = "2.3.0"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion,
  "org.apache.kafka" % "kafka-clients" % "1.1.0",
  "org.fusesource" % "sigar" % "1.6.4"
)

scalacOptions := Seq(
  "-Xlint",
  "-deprecation",
  "-feature",
  "-Xfatal-warnings"
)