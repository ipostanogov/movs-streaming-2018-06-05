package ru.psu.movs.statistics

import java.util.Properties

import org.apache.kafka.clients.producer._
import org.hyperic.sigar.Sigar
import org.json4s._
import org.json4s.jackson.JsonMethods._

object StatisticsWriter {
  def main(args: Array[String]) {
    val id = java.util.UUID.randomUUID().toString
    val kafkaParams = new Properties()
    kafkaParams.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092")
    kafkaParams.put(ProducerConfig.ACKS_CONFIG, "1")
    kafkaParams.put(ProducerConfig.RETRIES_CONFIG, "0")
    kafkaParams.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    kafkaParams.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, "org.apache.kafka.common.serialization.StringSerializer")
    val producer: Producer[String, String] = new KafkaProducer[String, String](kafkaParams)
    val sigar = new Sigar
    while (true) {
      import Statistics._
      val statistics = Statistics(cpu = sigar.getCpuPerc.getCombined * 100, mem = sigar.getMem.getUsedPercent)
      val jsonStr = pretty(render(Extraction.decompose(statistics)))
      val data = new ProducerRecord[String, String](Statistics.topic, id, jsonStr)
      producer.send(data)
      println(jsonStr)
      Thread.sleep(500)
    }
  }
}
