package ru.psu.movs.statistics

import java.nio.file.Files

import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010._
import org.apache.spark.streaming.{Seconds, State, StateSpec, StreamingContext}
import org.json4s._
import org.json4s.jackson.JsonMethods._

object StatisticsReader {
  val cpuLimit = 5

  def main(args: Array[String]) {
    Logger.getLogger("org").setLevel(Level.OFF)
    val conf = new SparkConf().setMaster("local[*]").setAppName("Kafka Streaming Reader Example")
    val ssc = new StreamingContext(conf, Seconds(1))
    ssc.checkpoint(Files.createTempDirectory(null).toString)
    val kafkaParams = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> "localhost:9092",
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[StringDeserializer],
      ConsumerConfig.GROUP_ID_CONFIG -> "StatisticsReader-group-id",
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "latest"
    )
    val messages = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](Array(Statistics.topic), kafkaParams)
    )
    import Statistics._
    val statisticsDStream = messages.map { kr => kr.key() -> parse(kr.value()).extract[Statistics] }

    statisticsDStream.print()

    val mappingFunc = (id: String, statistics: Option[Statistics], state: State[Int]) => {
      val highCpu = statistics.map(_.cpu).getOrElse(0.0) > 50.0
      if (highCpu)
        state.update(state.getOption().getOrElse(0) + 1)
      else
        state.update(0)
      id -> state.get()
    }

    statisticsDStream.mapWithState(StateSpec.function(mappingFunc)).filter { case (_, cpu) =>
      cpu > 0.0 && cpu % cpuLimit == 0
    }.mapValues("ALERT! High cpu " + _ + " times in a row").print()

    ssc.start()
    ssc.awaitTermination()
  }
}
