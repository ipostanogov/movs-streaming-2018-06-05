package ru.psu.movs.statistics

import org.json4s.FullTypeHints
import org.json4s.jackson.Serialization

case class Statistics(cpu: Double, mem: Double)

object Statistics {
  implicit val formats = {
    Serialization.formats(FullTypeHints(List(classOf[Statistics])))
  }

  val topic = "movs-stat-example"
}