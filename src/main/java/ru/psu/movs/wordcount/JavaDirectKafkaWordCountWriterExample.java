package ru.psu.movs.wordcount;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;
import java.util.Random;

public class JavaDirectKafkaWordCountWriterExample {
    public static void main(String[] args) throws InterruptedException {
        Properties kafkaParams = new Properties();
        kafkaParams.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        kafkaParams.put(ProducerConfig.ACKS_CONFIG, "all");
        kafkaParams.put(ProducerConfig.RETRIES_CONFIG, "0");
        kafkaParams.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        kafkaParams.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        Producer<String, String> producer = new KafkaProducer<>(kafkaParams);
        Random rnd = new Random();
        for (int i = 0; i < 100; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < 100; j++) {
                char nextChar = (char) (rnd.nextInt('z' - 'a') + 'a' + 1);
                sb.append(nextChar);
                ProducerRecord<String, String> data = new ProducerRecord<>(
                        "wcTopic", null, nextChar + "");
                producer.send(data);
            }
            System.out.println(sb.toString());
            Thread.sleep(500);
        }
    }
}
